<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/node_modules/bootstrap-material-design/dist/css/ripples.min.css">
    <link rel="stylesheet" href="/node_modules/bootstrap-material-design/dist/css/bootstrap-material-design.min.css">
    <link href="//fezvrasta.github.io/snackbarjs/dist/snackbar.min.css" rel="stylesheet">
    <style>
        body {
            transition: background-color .6s ease;
            -webkit-transition: background-color .6s ease;
            -moz-transition: background-color .6s ease;
        }
        .transform-none {
            text-transform: none;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="navbar navbar-default" style="margin-bottom: 0;">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="javascript:void(0)">Brand</a>
                    </div>
                    <div class="navbar-collapse collapse navbar-responsive-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="javascript:void(0)">Active</a></li>
                            <li><a href="javascript:void(0)">Link</a></li>
                            <li class="dropdown">
                                <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown
                                    <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)">Action</a></li>
                                    <li><a href="javascript:void(0)">Another action</a></li>
                                    <li><a href="javascript:void(0)">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li class="dropdown-header">Dropdown header</li>
                                    <li><a href="javascript:void(0)">Separated link</a></li>
                                    <li><a href="javascript:void(0)">One more separated link</a></li>
                                </ul>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-left">
                            <div class="form-group">
                                <input type="text" class="form-control col-md-8" placeholder="Search">
                            </div>
                        </form>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="javascript:void(0)">Link</a></li>
                            <li class="dropdown">
                                <a href="javascript:void(0)" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown
                                    <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)">Action</a></li>
                                    <li><a href="javascript:void(0)">Another action</a></li>
                                    <li><a href="javascript:void(0)">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="javascript:void(0)">Separated link</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="jumbotron">
                <div class="togglebutton text-right">
                    <label>
                        <input type="checkbox" id="switchTheme" checked> <span id="titleTheme">Theme: Light</span>
                    </label>
                </div>
                <div class="page-header text-center">
                    <h1>W3CProject documentation</h1>
                </div>
                <div class="docs-list">
                    <div class="docs-item">
                        <a href="#defaultValues"
                           data-toggle="collapse"
                           aria-expanded="false"
                           aria-controls="defaultValues"
                           class="h2 btn btn-default btn-lg btn-block btn-raised transform-none">_defaultValues</a>
                        <div class="collapse"
                             id="defaultValues">
                        <pre style='color:#000020;background:#f6f8ff;'><span style='color:#800000; '>"</span><span style='color:#1060b6; '>info</span><span style='color:#800000; '>"</span><span style='color:#406080; '>:</span> <span style='color:#406080; '>{</span>
        <span style='color:#800000; '>"</span><span style='color:#1060b6; '>form</span><span style='color:#800000; '>"</span><span style='color:#406080; '>:</span> <span style='color:#406080; '>{</span>
            <span style='color:#800000; '>"</span><span style='color:#1060b6; '>clearForm</span><span style='color:#800000; '>"</span><span style='color:#406080; '>:</span> <span style='color:#800000; '>"</span><span style='color:#1060b6; '>W3CProject Form success clear!</span><span style='color:#800000; '>"</span><span style='color:#308080; '>,</span>
            <span style='color:#800000; '>"</span><span style='color:#1060b6; '>submitForm</span><span style='color:#800000; '>"</span><span style='color:#406080; '>:</span> <span style='color:#800000; '>"</span><span style='color:#1060b6; '>W3CProject Form success submit!</span><span style='color:#800000; '>"</span><span style='color:#308080; '>,</span>
            <span style='color:#800000; '>"</span><span style='color:#1060b6; '>alertMessageShow</span><span style='color:#800000; '>"</span><span style='color:#406080; '>:</span> <span style='color:#800000; '>"</span><span style='color:#1060b6; '>W3CProject Message Alert Show!</span><span style='color:#800000; '>"</span><span style='color:#308080; '>,</span>
            <span style='color:#800000; '>"</span><span style='color:#1060b6; '>alertMessageHide</span><span style='color:#800000; '>"</span><span style='color:#406080; '>:</span> <span style='color:#800000; '>"</span><span style='color:#1060b6; '>W3CProject Message Alert Hide!</span><span style='color:#800000; '>"</span>
        <span style='color:#406080; '>}</span>
    <span style='color:#406080; '>}</span><span style='color:#308080; '>,</span>
    <span style='color:#800000; '>"</span><span style='color:#1060b6; '>errors</span><span style='color:#800000; '>"</span><span style='color:#406080; '>:</span> <span style='color:#406080; '>{</span>
        <span style='color:#800000; '>"</span><span style='color:#1060b6; '>form</span><span style='color:#800000; '>"</span><span style='color:#406080; '>:</span> <span style='color:#406080; '>{</span>
            <span style='color:#800000; '>"</span><span style='color:#1060b6; '>secureUndefined</span><span style='color:#800000; '>"</span><span style='color:#406080; '>:</span> <span style='color:#800000; '>"</span><span style='color:#1060b6; '>W3CProject FORM not send! Hidden input value not Exist!</span><span style='color:#800000; '>"</span><span style='color:#308080; '>,</span>
            <span style='color:#800000; '>"</span><span style='color:#1060b6; '>secureLength</span><span style='color:#800000; '>"</span><span style='color:#406080; '>:</span> <span style='color:#800000; '>"</span><span style='color:#1060b6; '>W3CProject FORM not send! Hidden input value (#w3c_security_input) not empty!</span><span style='color:#800000; '>"</span>
        <span style='color:#406080; '>}</span>
    <span style='color:#406080; '>}</span>
</pre>
                        </div>
                    </div>
                    <hr />
                    <div class="docs-item">
                        <a href="#ajaxSubmit"
                           data-toggle="collapse"
                           aria-expanded="false"
                           aria-controls="ajaxSubmit"
                           class="h2 btn btn-default btn-lg btn-block btn-raised transform-none">ajaxSubmit</a>
                        <div class="collapse"
                             id="ajaxSubmit">
                            <p>Send form with ajax</p>
                            <p>Default use:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>ajaxSubmit<span style='color:#308080; '>(</span>params<span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
</pre>
                            <p>Example:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>window<span style='color:#308080; '>.</span>onload <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>function</span><span style='color:#308080; '>(</span><span style='color:#308080; '>)</span> <span style='color:#406080; '>{</span>
    <span style='color:#200080; font-weight:bold; '>var</span> w3c <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>new</span> W3CProject<span style='color:#308080; '>(</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        form <span style='color:#308080; '>=</span> $<span style='color:#308080; '>(</span><span style='color:#800000; '>'</span><span style='color:#1060b6; '>#myForm</span><span style='color:#800000; '>'</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        urlToFile <span style='color:#308080; '>=</span> <span style='color:#800000; '>'</span><span style='color:#1060b6; '>/path/to/file.php</span><span style='color:#800000; '>'</span><span style='color:#406080; '>;</span>

    w3c<span style='color:#308080; '>.</span>ajaxSubmit<span style='color:#308080; '>(</span><span style='color:#406080; '>{</span>
        form<span style='color:#406080; '>:</span> form<span style='color:#308080; '>,</span>
        file<span style='color:#406080; '>:</span> urlToFile<span style='color:#308080; '>,</span>
        data<span style='color:#406080; '>:</span> <span style='color:#406080; '>{</span>id<span style='color:#406080; '>:</span> <span style='color:#008c00; '>2</span><span style='color:#406080; '>}</span><span style='color:#308080; '>,</span>
        success<span style='color:#406080; '>:</span> <span style='color:#200080; font-weight:bold; '>function</span> <span style='color:#308080; '>(</span><span style='color:#308080; '>)</span> <span style='color:#406080; '>{</span>
            <span style='color:#595979; '>// Your code here...</span>
        <span style='color:#406080; '>}</span><span style='color:#308080; '>,</span>
        clear<span style='color:#406080; '>:</span> <span style='color:#0f4d75; '>true</span><span style='color:#308080; '>,</span>
        secure<span style='color:#406080; '>:</span> <span style='color:#0f4d75; '>true</span>
    <span style='color:#406080; '>}</span><span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
<span style='color:#406080; '>}</span>
</pre>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">form</h3>
                                </div>
                                <div class="panel-body">
                                    Select your form with jQuery: <code>$('#myForm')</code> <br>
                                    Or Native javaScript: <code>document.getElementById('myForm')</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">file</h3>
                                </div>
                                <div class="panel-body">
                                    Path to file
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">data</h3>
                                </div>
                                <div class="panel-body">
                                    You can create an Object of key/value: <code>{name: 'John', age: 24}</code>
                                    <br>
                                    Or use any jQuery method, like: <code>serializeArray()</code>
                                    <br><br>
                                    Default: <code>form.serialize()</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">success</h3>
                                </div>
                                <div class="panel-body">
                                    Action after success form submit.
                                    <br><br>
                                    Default: <code>console.info(_defaultValues.info.form.submitForm)</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">clear</h3>
                                </div>
                                <div class="panel-body">
                                    Clear inputs and textarea fields values in form after success submit.
                                    <br>
                                    Only <code>true</code> or <code>false</code>
                                    <br><br>
                                    Default: <code>false</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">secure</h3>
                                </div>
                                <div class="panel-body">
                                    Protect form against spam
                                    <br>
                                    Only <code>true</code> or <code>false</code>
                                    <br><br>
                                    Default: <code>false</code>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="docs-item">
                        <a href="#loadContentHtml"
                           data-toggle="collapse"
                           aria-expanded="false"
                           aria-controls="loadContentHtml"
                           class="h2 btn btn-default btn-lg btn-block btn-raised transform-none">loadContentHtml</a>
                        <div class="collapse"
                             id="loadContentHtml">
                            <p>Load html content to selector</p>
                            <p>Default use:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>loadContentHtml<span style='color:#308080; '>(</span>selector<span style='color:#308080; '>,</span> html<span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
</pre>
                            <p>Example:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>window<span style='color:#308080; '>.</span>onload <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>function</span><span style='color:#308080; '>(</span><span style='color:#308080; '>)</span> <span style='color:#406080; '>{</span>
    <span style='color:#200080; font-weight:bold; '>var</span> w3c <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>new</span> W3CProject<span style='color:#308080; '>(</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        div <span style='color:#308080; '>=</span> $<span style='color:#308080; '>(</span><span style='color:#800000; '>'</span><span style='color:#1060b6; '>#div</span><span style='color:#800000; '>'</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        text <span style='color:#308080; '>=</span> <span style='color:#800000; '>'</span><span style='color:#1060b6; '>&lt;h1>My Html content&lt;/h1></span><span style='color:#800000; '>'</span><span style='color:#406080; '>;</span>

    w3c<span style='color:#308080; '>.</span>loadContentHtml<span style='color:#308080; '>(</span>div<span style='color:#308080; '>,</span> text<span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
<span style='color:#406080; '>}</span>
</pre>
                            <p>Params:</p>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">selector</h3>
                                </div>
                                <div class="panel-body">
                                    Choose selector for paste html content: <code>$('#selector')</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">html</h3>
                                </div>
                                <div class="panel-body">
                                    Some html text
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="docs-item">
                        <a href="#loadContentFile"
                           data-toggle="collapse"
                           aria-expanded="false"
                           aria-controls="loadContentFile"
                           class="h2 btn btn-default btn-lg btn-block btn-raised transform-none">loadContentFile</a>
                        <div class="collapse"
                             id="loadContentFile">
                            <p>Load content from file to selector</p>
                            <p>Default use:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>loadContentFile<span style='color:#308080; '>(</span>selector<span style='color:#308080; '>,</span> file<span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
</pre>
                            <p>Example:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>window<span style='color:#308080; '>.</span>onload <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>function</span><span style='color:#308080; '>(</span><span style='color:#308080; '>)</span> <span style='color:#406080; '>{</span>
    <span style='color:#200080; font-weight:bold; '>var</span> w3c <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>new</span> W3CProject<span style='color:#308080; '>(</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        div <span style='color:#308080; '>=</span> $<span style='color:#308080; '>(</span><span style='color:#800000; '>'</span><span style='color:#1060b6; '>#div</span><span style='color:#800000; '>'</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        file <span style='color:#308080; '>=</span> <span style='color:#800000; '>'</span><span style='color:#1060b6; '>/path/to/file.html</span><span style='color:#800000; '>'</span><span style='color:#406080; '>;</span>

    w3c<span style='color:#308080; '>.</span>loadContentFile<span style='color:#308080; '>(</span>div<span style='color:#308080; '>,</span> file<span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
<span style='color:#406080; '>}</span>
</pre>
                            <p>Params:</p>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">selector</h3>
                                </div>
                                <div class="panel-body">
                                    Choose selector for paste content from file: <code>$('#selector')</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">file</h3>
                                </div>
                                <div class="panel-body">
                                    Link to file
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="docs-item">
                        <a href="#clearForm"
                           data-toggle="collapse"
                           aria-expanded="false"
                           aria-controls="clearForm"
                           class="h2 btn btn-default btn-lg btn-block btn-raised transform-none">clearForm</a>
                        <div class="collapse"
                             id="clearForm">
                            <p>Clear form fields values</p>
                            <p>Default use:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>clearForm<span style='color:#308080; '>(</span>form<span style='color:#308080; '>,</span> selectors<span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
</pre>
                            <p>Example:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>window<span style='color:#308080; '>.</span>onload <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>function</span><span style='color:#308080; '>(</span><span style='color:#308080; '>)</span> <span style='color:#406080; '>{</span>
    <span style='color:#200080; font-weight:bold; '>var</span> w3c <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>new</span> W3CProject<span style='color:#308080; '>(</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        form <span style='color:#308080; '>=</span> $<span style='color:#308080; '>(</span><span style='color:#800000; '>'</span><span style='color:#1060b6; '>#myForm</span><span style='color:#800000; '>'</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        selectors <span style='color:#308080; '>=</span> <span style='color:#800000; '>'</span><span style='color:#1060b6; '>input[type="text"], input[type="email"], textarea</span><span style='color:#800000; '>'</span><span style='color:#406080; '>;</span>

    w3c<span style='color:#308080; '>.</span>clearForm<span style='color:#308080; '>(</span>form<span style='color:#308080; '>,</span> selectors<span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
<span style='color:#406080; '>}</span>
</pre>
                            <p>Params:</p>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">form</h3>
                                </div>
                                <div class="panel-body">
                                    Choose form selector: <code>$('#myForm')</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">selectors</h3>
                                </div>
                                <div class="panel-body">
                                    Fields list for the cleaning: <code>'input, textarea...'</code>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="docs-item">
                        <a href="#alertMessage"
                           data-toggle="collapse"
                           aria-expanded="false"
                           aria-controls="alertMessage"
                           class="h2 btn btn-default btn-lg btn-block btn-raised transform-none">alertMessage</a>
                        <div class="collapse"
                             id="alertMessage">
                            <p>Show and hide message with fade</p>
                            <p>Default use:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>alertMessage<span style='color:#308080; '>(</span>params<span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
</pre>
                            <p>Example:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>window<span style='color:#308080; '>.</span>onload <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>function</span><span style='color:#308080; '>(</span><span style='color:#308080; '>)</span> <span style='color:#406080; '>{</span>
    <span style='color:#200080; font-weight:bold; '>var</span> w3c <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>new</span> W3CProject<span style='color:#308080; '>(</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        form <span style='color:#308080; '>=</span> $<span style='color:#308080; '>(</span><span style='color:#800000; '>'</span><span style='color:#1060b6; '>#myForm</span><span style='color:#800000; '>'</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        event <span style='color:#308080; '>=</span> <span style='color:#800000; '>'</span><span style='color:#1060b6; '>submit</span><span style='color:#800000; '>'</span><span style='color:#308080; '>,</span>
        messageContainer <span style='color:#308080; '>=</span> $<span style='color:#308080; '>(</span><span style='color:#800000; '>'</span><span style='color:#1060b6; '>#messageContainer</span><span style='color:#800000; '>'</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        message <span style='color:#308080; '>=</span> <span style='color:#800000; '>'</span><span style='color:#1060b6; '>Form success submit, bye!</span><span style='color:#800000; '>'</span><span style='color:#406080; '>;</span>

    w3c<span style='color:#308080; '>.</span>alertMessage<span style='color:#308080; '>(</span><span style='color:#406080; '>{</span>
        target<span style='color:#406080; '>:</span> form<span style='color:#308080; '>,</span>
        event<span style='color:#406080; '>:</span> <span style='color:#800000; '>'</span><span style='color:#1060b6; '>submit</span><span style='color:#800000; '>'</span><span style='color:#308080; '>,</span>
        selector<span style='color:#406080; '>:</span> messageContainer<span style='color:#308080; '>,</span>
        message<span style='color:#406080; '>:</span> message<span style='color:#308080; '>,</span>
        timeOut<span style='color:#406080; '>:</span> <span style='color:#008c00; '>3000</span><span style='color:#308080; '>,</span>
        speed<span style='color:#406080; '>:</span> <span style='color:#008c00; '>400</span>
    <span style='color:#406080; '>}</span><span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
<span style='color:#406080; '>}</span>
</pre>
                            <p>Params:</p>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">target</h3>
                                </div>
                                <div class="panel-body">
                                    Select target for EventListener: <code>$('#myForm'), $('a.myLink')</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">event</h3>
                                </div>
                                <div class="panel-body">
                                    Event for EventListener: <code>'submit', 'click'...</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">selector</h3>
                                </div>
                                <div class="panel-body">
                                    Selector for output Alert Message: <code>$('#mySelector')</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">message</h3>
                                </div>
                                <div class="panel-body">
                                    Alert Message text for <code>selector</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">timeOut</h3>
                                </div>
                                <div class="panel-body">
                                    Hide message after some time: <code>(int) 4000 (4 seconds)</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">speed</h3>
                                </div>
                                <div class="panel-body">
                                    Speed of appearance and disappearance: <code>(int) 4000 (4 seconds)</code>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="docs-item">
                        <a href="#fastFor"
                           data-toggle="collapse"
                           aria-expanded="false"
                           aria-controls="fastFor"
                           class="h2 btn btn-default btn-lg btn-block btn-raised transform-none">fastFor</a>
                        <div class="collapse"
                             id="fastFor">
                            <p>Short for construct</p>
                            <p>Default use:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>fastForm<span style='color:#308080; '>(</span>array<span style='color:#308080; '>,</span> callback<span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
</pre>
                            <p>Example:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>window<span style='color:#308080; '>.</span>onload <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>function</span><span style='color:#308080; '>(</span><span style='color:#308080; '>)</span> <span style='color:#406080; '>{</span>
    <span style='color:#200080; font-weight:bold; '>var</span> w3c <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>new</span> W3CProject<span style='color:#308080; '>(</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        myArray <span style='color:#308080; '>=</span> <span style='color:#308080; '>[</span><span style='color:#800000; '>'</span><span style='color:#1060b6; '>First</span><span style='color:#800000; '>'</span><span style='color:#308080; '>,</span> <span style='color:#800000; '>'</span><span style='color:#1060b6; '>Second</span><span style='color:#800000; '>'</span><span style='color:#308080; '>,</span> <span style='color:#800000; '>'</span><span style='color:#1060b6; '>Third</span><span style='color:#800000; '>'</span><span style='color:#308080; '>.</span><span style='color:#308080; '>.</span><span style='color:#308080; '>.</span><span style='color:#308080; '>]</span><span style='color:#406080; '>;</span>

    w3c<span style='color:#308080; '>.</span>fastFor<span style='color:#308080; '>(</span>myArray<span style='color:#308080; '>,</span> <span style='color:#200080; font-weight:bold; '>function</span><span style='color:#308080; '>(</span><span style='color:#200080; font-weight:bold; '>item</span><span style='color:#308080; '>)</span> <span style='color:#406080; '>{</span>
        console<span style='color:#308080; '>.</span><span style='color:#200080; font-weight:bold; '>log</span><span style='color:#308080; '>(</span><span style='color:#200080; font-weight:bold; '>item</span><span style='color:#308080; '>)</span><span style='color:#406080; '>;</span> <span style='color:#595979; '>// return console log all items from array</span>
    <span style='color:#406080; '>}</span><span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
<span style='color:#406080; '>}</span>
</pre>
                            <p>Params:</p>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">array</h3>
                                </div>
                                <div class="panel-body">
                                    Array list: <code>['First', 'Second', 'Third'...]</code>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">callback</h3>
                                </div>
                                <div class="panel-body">
                                    You can use it like you want: <br>
                                    <code>function(item) { console.log(item) }</code>
                                    <br>
                                    <code>function(item) { otherArray.push(item) }</code>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="docs-item">
                        <a href="#isDisabled"
                           data-toggle="collapse"
                           aria-expanded="false"
                           aria-controls="isDisabled"
                           class="h2 btn btn-default btn-lg btn-block btn-raised transform-none">isDisabled</a>
                        <div class="collapse"
                             id="isDisabled">
                            <p>Check if element is disabled (return boolean)</p>
                            <p>Default use:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>isDisabled<span style='color:#308080; '>(</span>selector<span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
</pre>
                            <p>Example:</p>
                            <pre style='color:#000020;background:#f6f8ff;'>window<span style='color:#308080; '>.</span>onload <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>function</span><span style='color:#308080; '>(</span><span style='color:#308080; '>)</span> <span style='color:#406080; '>{</span>
    <span style='color:#200080; font-weight:bold; '>var</span> w3c <span style='color:#308080; '>=</span> <span style='color:#200080; font-weight:bold; '>new</span> W3CProject<span style='color:#308080; '>(</span><span style='color:#308080; '>)</span><span style='color:#308080; '>,</span>
        selector <span style='color:#308080; '>=</span> $<span style='color:#308080; '>(</span><span style='color:#800000; '>'</span><span style='color:#1060b6; '>#mySelector</span><span style='color:#800000; '>'</span><span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>

    w3c<span style='color:#308080; '>.</span>isDisabled<span style='color:#308080; '>(</span>selector<span style='color:#308080; '>)</span><span style='color:#406080; '>;</span> <span style='color:#595979; '>// return true/false</span>
<span style='color:#406080; '>}</span>
</pre>
                            <p>Params:</p>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">selector</h3>
                                </div>
                                <div class="panel-body">
                                    Element you want to check: <code>$('#mySelector')</code>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="docs-item">
                        <a href="#debug"
                           data-toggle="collapse"
                           aria-expanded="false"
                           aria-controls="debug"
                           class="h2 btn btn-default btn-lg btn-block btn-raised transform-none">debug</a>
                        <div class="collapse"
                             id="debug">
                            <p>Default use:</p>
                            soon
                            <p>Example:</p>
                            soon
                            <p>Params:</p>
                            soon
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
<script src="/ajax/libs/1.0/dev/w3c.dev.js"></script>
<script src="/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="//fezvrasta.github.io/snackbarjs/dist/snackbar.min.js"></script>
<script src="/node_modules/bootstrap-material-design/dist/js/ripples.min.js"></script>
<script src="/node_modules/bootstrap-material-design/dist/js/material.min.js"></script>
<script>
    $(function() {
        $.material.init();
        var w3c = new W3CProject(),
            switchTheme = $('#switchTheme'),
            titleTheme = $('#titleTheme');

        switchTheme.on('click', function () {
            if(w3c.isChecked(switchTheme)) {
                $('body').css('background-color', '#eee');
                titleTheme.text('Theme: Light');
            } else {
                $('body').css('background-color', '#333');
                titleTheme.text('Theme: Dark');
            }
        });
    });
</script>
</body>
</html>