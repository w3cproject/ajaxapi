<?
$ajaxApiPath = '/ajax/libs/1.0/dist/w3c.min.js';
$cdnApiPath = 'https://cdn.jsdelivr.net/w3cproject/1.0/w3c.min.js';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>W3CProject APIs</title>
    <meta name="robots" content="none">
    <meta name="description"
          content="A free responsive website template made exclusively for Codrops by Pixel Buddha and PSD2HTML">
    <meta name="keywords" content="website template, css3, one page, bootstrap, app template, web app, start-up">
    <meta name="author" content="Pixel Buddha and PSD2HTML for Codrops">
    <link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicons/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicons/manifest.json">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#603cba">
    <meta name="msapplication-TileImage" content="favicons/mstile-144x144.png">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="/node_modules/bootstrap-material-design/dist/css/ripples.min.css">
    <link rel="stylesheet" href="/node_modules/bootstrap-material-design/dist/css/bootstrap-material-design.min.css">
    <link href="//fezvrasta.github.io/snackbarjs/dist/snackbar.min.css" rel="stylesheet">
    <link rel="stylesheet" href="fonts/font-awesome-4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/all.css">
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700|Source+Sans+Pro:400,700,400italic,700italic'
          rel='stylesheet' type='text/css'>
</head>
<body>
<div id="wrapper">
    <header id="header">
        <div class="container">
            <div class="logo">
                <a href="/" style="font-size: 24px;font-weight: bold;">
                    <span style="color: #8D81AC;">W3C</span><span style="color: #333333;">Project</span>

                </a>
            </div>
            <nav id="nav">
                <div class="opener-holder">
                    <a href="#" class="nav-opener"><span></span></a>
                </div>
                <a href="<?= $cdnApiPath ?>"
                   class="btn btn-raised btn-primary rounded"
                   target="_blank"
                   style="color: #fff;"
                   rel="nofollow">Download v 1.0</a>
                <div class="nav-drop">
                    <ul>
                        <li class="active visible-sm visible-xs"><a href="#">Home</a></li>
                        <li><a href="/docs/" target="_blank">Documentation</a></li>
                        <li><a href="#">About Fork</a></li>
                        <li><a href="#">Buying Options</a></li>
                        <li><a href="#">Support</a></li>
                    </ul>
                    <div class="drop-holder visible-sm visible-xs">
                        <span>Follow Us</span>
                        <ul class="social-networks">
                            <li><a class="fa fa-github" href="#"></a></li>
                            <li><a class="fa fa-twitter" href="#"></a></li>
                            <li><a class="fa fa-facebook" href="#"></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <section class="visual">
        <div class="container">
            <div class="text-block">
                <div class="heading-holder">
                    <h1>W3CProject</h1>
                </div>
                <p class="tagline">AJAX API for faster web development</p>
                <span class="info">Version 1.0</span>
            </div>
        </div>
        <img src="images/img-decor-01.jpg" alt="" class="bg-stretch">
    </section>
    <section class="main">
        <div class="container">
            <div id="cta">
                <a href="<?= $cdnApiPath ?>"
                   class="btn btn-raised btn-primary rounded"
                   target="_blank"
                   rel="nofollow">Download v 1.0</a>
                <p>W3CProject AJAX lib for faster development</p>
            </div>
            <div class="row">
                <div class="text-box col-md-offset-1 col-md-10">
                    <h2>Get started</h2>
                    <p>Insert code into the <code>head</code> section</p>
                    <pre style='color:#000020;background:#f6f8ff;text-align: left;'><span style='color:#595979; '>/*</span>
<span style='color:#595979; '>&#xa0;aJax API W3CProject v1.0 (http://w3cproject.xyz/)</span>
<span style='color:#595979; '>&#xa0;git repository: https://github.com/w3cproject/ajaxapi.git</span>
<span style='color:#595979; '>&#xa0;*/</span>
<span style='color:#308080; '>(</span><span style='color:#200080; font-weight:bold; '>function</span><span style='color:#308080; '>(</span><span style='color:#308080; '>)</span><span style='color:#406080; '>{</span><span style='color:#200080; font-weight:bold; '>var</span> a<span style='color:#308080; '>=</span>document<span style='color:#308080; '>.</span>createElement<span style='color:#308080; '>(</span><span style='color:#800000; '>"</span><span style='color:#1060b6; '>script</span><span style='color:#800000; '>"</span><span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>a<span style='color:#308080; '>.</span>type<span style='color:#308080; '>=</span><span style='color:#800000; '>"</span><span style='color:#1060b6; '>text/javascript</span><span style='color:#800000; '>"</span><span style='color:#406080; '>;</span>
a<span style='color:#308080; '>.</span>src<span style='color:#308080; '>=</span><span style='color:#800000; '>"</span><span style='color:#1060b6; '>https://cdn.jsdelivr.net/w3cproject/1.0/w3c.min.js</span><span style='color:#800000; '>"</span><span style='color:#406080; '>;</span>a<span style='color:#308080; '>.</span>async<span style='color:#308080; '>=</span><span style='color:#308080; '>!</span><span style='color:#008c00; '>0</span><span style='color:#406080; '>;</span>
document<span style='color:#308080; '>.</span>getElementsByTagName<span style='color:#308080; '>(</span><span style='color:#800000; '>"</span><span style='color:#1060b6; '>head</span><span style='color:#800000; '>"</span><span style='color:#308080; '>)</span><span style='color:#308080; '>[</span><span style='color:#008c00; '>0</span><span style='color:#308080; '>]</span><span style='color:#308080; '>.</span>appendChild<span style='color:#308080; '>(</span>a<span style='color:#308080; '>)</span><span style='color:#406080; '>}</span><span style='color:#308080; '>)</span><span style='color:#308080; '>(</span><span style='color:#308080; '>)</span><span style='color:#406080; '>;</span>
</pre>
                    <p>Or just insert before close <code>body</code> tag</p>
                    <pre style='color:#000020;background:#f6f8ff;text-align: left;'><span style='color:#0057a6; '>&lt;</span><span style='color:#200080; font-weight:bold; '>script</span><span style='color:#474796; '> async</span>
<span style='color:#474796; '>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span style='color:#074726; '>defer</span><span style='color:#474796; '></span>
<span style='color:#474796; '>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</span><span style='color:#074726; '>src</span><span style='color:#308080; '>=</span><span style='color:#1060b6; '>"https://cdn.jsdelivr.net/w3cproject/1.0/w3c.min.js"</span><span style='color:#474796; '> </span>
<span style='color:#474796; '>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;integrity</span><span style='color:#308080; '>=</span><span style='color:#1060b6; '>"sha384-EL0fkRpJdUcbnzdUggdPKXKjvr+bnoIKGmyZmiv94Ll+s02SNIoOneVZEaStsafy"</span><span style='color:#474796; '></span>
<span style='color:#474796; '>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;crossorigin</span><span style='color:#308080; '>=</span><span style='color:#1060b6; '>"anonymous"</span><span style='color:#0057a6; '>></span><span style='color:#0057a6; '>&lt;/</span><span style='color:#200080; font-weight:bold; '>script</span><span style='color:#0057a6; '>></span>
</pre>
                    <div class="social-placeholder">
                        <!-- Place this tag where you want the button to render. -->
                        <a class="github-button"
                           href="https://github.com/w3cproject"
                           data-style="mega"
                           data-count-href="/w3cproject/followers"
                           data-count-api="/users/w3cproject#followers"
                           data-count-aria-label="# followers on GitHub"
                           aria-label="Follow @w3cproject on GitHub">Follow @w3cproject</a>

                        <a class="github-button"
                           href="https://github.com/w3cproject/ajaxapi/archive/master.zip"
                           data-style="mega"
                           aria-label="Download w3cproject/ajaxapi on GitHub">Download</a>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="area">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <h2 class="visible-xs visible-sm text-primary">&lt;Here is what you get&gt;</h2>
                    <ul class="visual-list">
                        <li>
                            <div class="img-holder">
                                <img src="images/graph-04.svg" width="110" alt="">
                            </div>
                            <div class="text-holder">
                                <h3>Created to Make A Better Web</h3>
                                <p>Aenean cursus imperdiet nisl id fermentum. Aliquam pharetra dui laoreet vulputate
                                    dignissim. Sed id metus id quam auctor molestie eget ut augue. </p>
                            </div>
                        </li>
                        <li>
                            <div class="img-holder">
                                <img class="pull-left" src="images/graph-03.svg" width="90" alt="">
                            </div>
                            <div class="text-holder">
                                <h3>Infinite Customization</h3>
                                <p>Maecenas eu dictum felis, a dignissim nibh. Mauris rhoncus felis odio, ut volutpat
                                    massa placerat ac. Curabitur dapibus iaculis mi gravida luctus. Aliquam erat
                                    volutpat. </p>
                            </div>
                        </li>
                        <li>
                            <div class="img-holder">
                                <img src="images/graph-02.svg" height="84" alt="">
                            </div>
                            <div class="text-holder">
                                <h3>Experimental Features</h3>
                                <p>Maecenas eu dictum felis, a dignissim nibh. Mauris rhoncus felis odio, ut volutpat
                                    massa placerat ac. Curabitur dapibus iaculis mi gravida luctus. Aliquam erat
                                    volutpat. </p>
                            </div>
                        </li>
                        <li>
                            <div class="img-holder">
                                <img src="images/graph-01.svg" height="71" alt="">
                            </div>
                            <div class="text-holder">
                                <h3>Time-Saving Power Tools</h3>
                                <p>Maecenas eu dictum felis, a dignissim nibh. Mauris rhoncus felis odio, ut volutpat
                                    massa placerat ac. Curabitur dapibus iaculis mi gravida luctus. Aliquam erat
                                    volutpat. </p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-7">
                    <div class="slide-holder">
                        <h2 class="hidden-xs hidden-sm text-primary">&lt;Here is what you get&gt;</h2>
                        <div class="img-slide scroll-trigger"><img src="images/lib-screen.jpg" height="624" width="1184"
                                                                   alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="main">
        <div class="container">
            <div class="block-holder">
                <div class="block-frame">
                    <div class="cycle-gallery">
                        <div class="mask">
                            <div class="slideset">
                                <!--<div class="slide">
                                    <div class="img-box">
                                        <div class="img-holder"><img src="images/w3c-mini.jpg" height="85" width="85"
                                                                     alt=""></div>
                                        <div class="text-holder">
                                            <h3>Name Name</h3>
                                            <a href="https://github.com/w3cproject"
                                               target="_blank"
                                               rel="nofollow">@W3CProject</a>
                                        </div>
                                    </div>
                                    <p><a href="#">@Pixelbuddha</a> Suspendisse sodales sem est, in scelerisque felis
                                        scelerisque in. Aenean faucibus mollis risus. Praesent sit amet erat eget eros.
                                    </p>
                                    <em class="date">2 hours ago</em>
                                </div>-->
                                <div class="slide">
                                    <div class="img-box">
                                        <div class="img-holder"><img src="images/user-02.jpg" height="85" width="85"
                                                                     alt=""></div>
                                        <div class="text-holder">
                                            <h3>Shane Jennings</h3>
                                            <a href="#">@shanejen</a>
                                        </div>
                                    </div>
                                    <p><a href="#">@PSD2HTML</a> Ut id porta quam. Morbi sit amet magna lobortis,
                                        hendrerit lorem et, tincidunt lorem. Sed vulputate condimentum lorem vel dapibus
                                    </p>
                                    <em class="date">5 hours ago</em>
                                </div>
                                <div class="slide">
                                    <div class="img-box">
                                        <div class="img-holder"><img src="images/user-03.jpg" height="85" width="85"
                                                                     alt=""></div>
                                        <div class="text-holder">
                                            <h3>Rafael Belverde</h3>
                                            <a href="#">@bellraffi</a>
                                        </div>
                                    </div>
                                    <p><a href="#">@codrops</a> Curabitur nec dapibus ligula. In eget ante in nisi
                                        laoreet accumsan pretium vitae est. Nulla lacinia efficitur dui eget accumsan?
                                    </p>
                                    <em class="date">1 day ago</em>
                                </div>
                            </div>
                        </div>
                        <a class="btn-prev" href="#"><i class="glyphicon glyphicon-menu-left"></i></a>
                        <a class="btn-next" href="#"><i class="glyphicon glyphicon-menu-right"></i></a>
                    </div>
                </div>
                <div class="block-frame">
                    <ul class="cta-list">
                        <li>
                            <a href="javascript:void(0);" class="btn btn-default"><i class="fa fa-github"></i> GitHub</a>
                            <p>7 352 Followers</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="btn btn-info"><i class="fa fa-twitter"></i> Twitter</a>
                            <p>136 312 Followers</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="btn btn-blue"><i class="fa fa-facebook"></i> Facebook</a>
                            <p>218 092 Subscribers</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--<section class="visual-container">
        <div class="visual-area">
            <div class="container">
                <h2>Where it is used</h2>
                <ul class="testimonials">
                    <li>
                        <div class="img-holder"><a href="#"><img src="images/logo-smashing.png" height="43" width="165"
                                                                 alt="smashing magazine"></a></div>
                        <p><em>Sed vestibulum scelerisque urna, eu finibus leo facilisis sit amet. Proin id dignissim
                                magna. Sed varius urna et pulvinar venenatis. </em></p>
                    </li>
                    <li>
                        <div class="img-holder"><a href="#"><img src="images/logo-codrops.png" height="50" width="148"
                                                                 alt="codrops"></a></div>
                        <p><em>Donec euismod dolor ut ultricies consequat. Vivamus urna ipsum, rhoncus molestie neque
                                ac, mollis eleifend nibh.</em></p>
                    </li>
                    <li>
                        <div class="img-holder"><a href="#"><img src="images/logo-w.png" height="64" width="64" alt="w"></a>
                        </div>
                        <p><em>In efficitur in velit et tempus. Duis nec odio dapibus, suscipit erat fringilla,
                                imperdiet nibh. Morbi tempus auctor felis ac vehicula. </em></p>
                    </li>
                    <li>
                        <div class="img-holder"><a href="#"><img src="images/logo-pixel.png" height="24" width="225"
                                                                 alt="Pixel Buddha"></a></div>
                        <p><em>Sed vestibulum scelerisque urna, eu finibus leo facilisis sit amet. Proin id dignissim
                                magna. Sed varius urna et pulvinar venenatis. </em></p>
                    </li>
                    <li>
                        <div class="img-holder"><a href=""><img src="images/logo-cb.png" height="34" width="166"
                                                                alt="creative bloq"></a></div>
                        <p><em>Praesent ut eros tristique, malesuada lectus vel, lobortis massa. Nulla faucibus lorem id
                                arcu consequat faucibus. </em></p>
                    </li>
                    <li>
                        <div class="img-holder"><a href="#"><img src="images/logo-tnw.png" height="34" width="108"
                                                                 alt="tnw"></a></div>
                        <p><em>Fusce pharetra erat id odio blandit, nec pharetra eros venenatis. Pellentesque porttitor
                                cursus massa et vestibulum.</em></p>
                    </li>
                </ul>
            </div>
            <img src="images/img-decor-02.jpg" height="764" width="1380" alt="" class="bg-stretch">
        </div>
        <div class="visual-area">
            <div class="container">
                <h2>Fork Subscription Pricing</h2>
                <div class="pricing-tables">
                    <div class="plan">
                        <div class="head">
                            <h3>Students</h3>
                        </div>
                        <div class="price">
                            <span class="price-main"><span class="symbol">$</span>8</span>
                            <span class="price-additional">per month</span>
                        </div>
                        <ul class="item-list">
                            <li>Personal License</li>
                        </ul>
                        <button type="button" class="btn btn-default rounded">purchase</button>
                    </div>
                    <div class="plan">
                        <div class="head">
                            <h3>professional</h3></div>
                        <div class="price">
                            <span class="price-main"><span class="symbol">$</span>19</span>
                            <span class="price-additional">per month</span>
                        </div>
                        <ul class="item-list">
                            <li>Professional License</li>
                            <li>Email Support</li>
                        </ul>
                        <button type="button" class="btn btn-default rounded">purchase</button>
                    </div>
                    <div class="plan recommended">
                        <div class="head">
                            <h3>agency</h3></div>
                        <div class="price">
                            <span class="price-main"><span class="symbol">$</span>49</span>
                            <span class="price-additional">per month</span>
                        </div>
                        <ul class="item-list">
                            <li>1-12 Team Members</li>
                            <li>Phone Support</li>
                        </ul>
                        <button type="button" class="btn btn-default rounded">purchase</button>
                    </div>
                    <div class="plan">
                        <div class="head">
                            <h3>enterprise</h3></div>
                        <div class="price">
                            <span class="price-main"><span class="symbol">$</span>79</span>
                            <span class="price-additional">per month</span>
                        </div>
                        <ul class="item-list">
                            <li>Unlimited Team Members</li>
                            <li>24/ 7 Phone Support</li>
                        </ul>
                        <button type="button" class="btn btn-default rounded">purchase</button>
                    </div>
                </div>
                <p class="silent">Duis lobortis arcu sed arcu tincidunt feugiat. Nulla nisi mauris, facilisis vitae
                    aliquet id, imperdiet quis nibh. Donec eget elit eu libero tincidunt consequat nec elementum orci.
                    Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
            </div>
            <img src="images/img-decor-03.jpg" height="1175" width="1380" alt="" class="bg-stretch">
        </div>
    </section>-->
    <section class="area">
        <div class="container">
            <div class="subscribe">
                <h3>Subscribe to Our Newsletter</h3>
                <form class="form-inline" id="subscribe_form">
                    <button type="submit" class="btn btn-primary rounded">Subscribe</button>
                    <div class="form-group" style="margin-top: 0;">
                        <input type="email"
                               class="form-control rounded"
                               required
                               id="exampleInputEmail2"
                               placeholder="Email...">
                    </div>
                </form>
                <div id="mm" style="display: none;"></div>
            </div>
        </div>
    </section>
    <section class="main">
        <div class="container">
            <div class="content">
                <div class="row">
                    <div class="col-md-4">
                        <h3>New in Release 2.8</h3>
                        <h4>Fully Available: Live Preview Support for Multiple Browsers</h4>
                        <p>Morbi faucibus ante ipsum, a tincidunt libero posuere et. Etiam tempor tortor at odio
                            condimentum, sit amet fringilla ligula maximus. Mauris venenatis nisl nisi, et dictum dui
                            aliquet nec. Sed erat, nec aliquam vel, aliquam ac felis. </p>
                        <h4>Improved Support for Typing </h4>
                        <p>Nulla purus quam, pulvinar sed ante et, feugiat maximus velit. Donec eu elit mauris. </p>
                        <h4>Improved Stability</h4>
                        <p>Proin id ligula eget massa tincidunt molestie. Morbi urna lectus, molestie vel quam vel,
                            iaculis fermentum nunc. Morbi imperdiet, leo in interdum pretium.</p>
                        <div class="btn-holder">
                            <a href="#" class="btn btn-link">Go to Release Log</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h3>Popular Extensions</h3>
                        <h4><a href="#">Custom Themes 1.1</a></h4>
                        <p>Etiam viverra vulputate diam, pulvinar cursus augue egestas ullamcorper. </p>
                        <h4><a href="#">Filter Forge</a></h4>
                        <p>Nulla purus quam, pulvinar sed et, feugiat maximus velit.</p>
                        <h4><a href="#">WebZap</a></h4>
                        <p>Morbi imperdiet, leo in interdum pretium, elit eros dapibus velit, eu posuere quam diam vitae
                            orci. Suspendisse interdum accumsan magna vitae commodo.</p>
                        <h4><a href="#">Renamy</a></h4>
                        <p>Vivamus consectetur suscipit elit, ut lacinia diam elementum et.</p>
                        <div class="btn-holder">
                            <a href="#" class="btn btn-link">Go to Marketplace</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h3>New From the Blog</h3>
                        <h4><a href="#">Morbi imperdiet, leo in interdum pretium.</a></h4>
                        <time class="date" datetime="2015-02-18">February 18, 2015</time>
                        <h4><a href="#">Donec orci ante, porta vel nulla quis, aliquet hendrerit leo.</a></h4>
                        <time class="date" datetime="2015-02-12">February 12, 2015</time>
                        <h4><a href="#">Suspendisse egestas vulputate luctus.</a></h4>
                        <time class="date" datetime="2015-02-06">February 6, 2015</time>
                        <h4><a href="#">Quisque varius ante lorem, eget pretium purus hendrerit a egestas. </a></h4>
                        <time class="date" datetime="2015-02-02">February 2, 2015</time>
                        <h4><a href="#">Phasellus in augue risus. </a></h4>
                        <time class="date" datetime="2015-01-23">January 23, 2015</time>
                        <div class="btn-holder">
                            <a href="#" class="btn btn-link">Go to Blog</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer id="footer">
        <div class="container">
            <div class="footer-holder">
                <div class="row">
                    <div class="col-md-4">
                        <div class="logo">
                            <a href="#" style="font-size: 24px;font-weight: bold;">
                                <span style="color: #8D81AC;">W3C</span><span style="color: #333333;">Project</span>
                            </a>
                        </div>
                        <p>Lorem ipsum dolor sit amet, feugiat delicat<br/> liberavisse id cum no quo.</p>
                    </div>
                    <div class="col-md-2">
                        <h4>Navigation</h4>
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Overview</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Buying Options</a></li>
                            <li><a href="#">Support</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="text-holder">
                            <strong class="phone"><a href="tel:3475677890">347 567 78 90</a></strong>
                            <span class="available">Available from 12PM - 18PM</span>
                            <h4>New York, NY</h4>
                            <address>560 Judah St &amp; 15th Ave, Apt 5 San Francisco, CA, 230903</address>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="text-frame">
                            <h4>Info</h4>
                            <p>Wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl
                                ut aliquip ex commodo consequat. Autem vel hendrerit iriure dolor in hendrerit.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="/node_modules/jquery/dist/jquery.min.js"></script>
<script src="/ajax/libs/1.0/dev/w3c.dev.js"></script>
<script src="js/bootstrap.js"></script>
<script src="//fezvrasta.github.io/snackbarjs/dist/snackbar.min.js"></script>
<script src="/node_modules/bootstrap-material-design/dist/js/ripples.min.js"></script>
<script src="/node_modules/bootstrap-material-design/dist/js/material.min.js"></script>
<script>
    $(function() {
        $.material.init();
    });
</script>
<script src="js/jquery.main.js"></script>
<script src="js/scripts.js"></script>
<script async defer src="https://buttons.github.io/buttons.js"></script>
<script>
    /*(function(){var a=document.createElement("script");a.type="text/javascript";
     a.src="http://api.w3cproject.xyz/ajax/libs/1.0/dist/w3c.min.js";
     a.async=!0;document.getElementsByTagName("body")[0].appendChild(a)})();*/
</script
</body>
</html>