$(function() {
    var w3c = new W3CProject(),
        form = $('#subscribe_form'),
        urlToFile = '',
        inputCopy = $('.input-copy');

    w3c.ajaxSubmit({
        form: form,
        file: urlToFile,
        success: function () {
            console.log('custom');
        },
        clear: true,
        secure: true
    }).alertMessage({
        target: form,
        event: 'submit',
        selector: $('#mm'),
        message: 'Form success',
        timeOut: 3000,
        speed: 400
    });
});