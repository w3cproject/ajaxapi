/*!
 * aJax API W3CProject v1.0 (http://w3cproject.xyz/)
 * git repository: https://github.com/w3cproject/ajaxapi.git
 */

"use strict";

/**
 * W3CProject Object
 */

function W3CProject() {

}

/**
 * Default values
 * @param {Object} defaultValues
 */

W3CProject.prototype._defaultValues = {
    "info": {
        "form": {
            "clearForm": "W3CProject Form success clear!",
            "submitForm": "W3CProject Form success submit!",
            "alertMessageShow": "W3CProject Message Alert Show!",
            "alertMessageHide": "W3CProject Message Alert Hide!"
        }
    },
    "errors": {
        "form": {
            "secureUndefined": "W3CProject FORM not send! Hidden input value not Exist!",
            "secureLength": "W3CProject FORM not send! Hidden input value (#w3c_security_input) not empty!"
        }
    }
};

/**
 * Send form with ajax and clearForm
 * @param {Object} params
 */

W3CProject.prototype.ajaxSubmit = function (params) {
    var _ = this,
        defaultValues = _._defaultValues,
        defaultSuccess = function (response) {
            console.info(defaultValues.info.form.submitForm);
        },
        defaultError = function (response) {
            console.error('Form has errors: ', response.statusText);
        },
        form = params.form,
        url = params.url,
        data = params.data,
        success = params.success || defaultSuccess,
        error = params.error || defaultError,
        clear = params.clear || false,
        secure = params.secure || false;

    if (secure === true) {
        var hiddenInput = document.createElement('input');
        hiddenInput.type = 'hidden';
        hiddenInput.className = 'w3c_security_input';
        form.append(hiddenInput);
    }

    form.submit(function (e) {
        if (secure === true && typeof hiddenInput === 'undefined') {
            console.error(defaultValues.error.form.secureUndefined);
        } else if (typeof hiddenInput !== 'undefined' && hiddenInput.value.length > 0) {
            console.error(defaultValues.error.form.secureLength);
        } else {
            $.ajax({
                url: url,
                method: 'POST',
                data: data || form.serialize(),
                success: function (response) {
                    success();
                    defaultSuccess();
                },
                error: error
            });

            if (clear === true) {
                _.clearForm(form, 'input, textarea');

                console.info(defaultValues.info.form.clearForm);
            }
        }

        e.preventDefault();
    });

    return _;
};

/**
 * Load html content to selector
 * @param {Object} selector
 * @param {Object} html
 */

W3CProject.prototype.loadContentHtml = function (selector, html) {
    selector[0].innerHTML = html;
};

/**
 * Load content from file to selector
 * @param {Object} selector
 * @param {Object} file
 */

W3CProject.prototype.loadContentFile = function (selector, file) {
    selector.load(file);
};

/**
 * Clear form fields values
 * @param {Object} form
 * @param {String} selectors
 */

W3CProject.prototype.clearForm = function (form, selectors) {
    var inputs = form.find(selectors);

    for (var i = 0; i < inputs.length; i++) {
        var input = inputs[i];

        if (input.type !== 'submit') {
            input.value = '';
        }
    }
};

/**
 * Show and hide message with fade
 * @param {Object} params
 */

W3CProject.prototype.alertMessage = function (params) {
    var _ = this,
        defaultValues = _._defaultValues,
        target = params.target,
        event = params.event || 'click',
        selector = params.selector,
        message = params.message || 'Form success send',
        timeOut = params.timeOut || 4000,
        speed = params.speed || 400;

    function printMsg() {
        selector.text(message);

        selector.fadeIn(speed);

        console.info(defaultValues.info.form.alertMessageShow);

        setTimeout(function () {
            selector.fadeOut(speed);
            console.info(defaultValues.info.form.alertMessageHide);
        }, timeOut);
    }

    target[0].addEventListener(event, function () {
        printMsg();
    });

    return this;
};

/**
 * Short for construct
 * @param {Object} array
 * @param {Object} callback
 */

W3CProject.prototype.fastFor = function (array, callback) {
    for (var i = 0; i < array.length; i++) {
        callback(array[i]);
    }
};

/**
 * Check if element is disabled (return boolean)
 * @param {Object} selector
 */

W3CProject.prototype.isDisabled = function (selector) {
    return selector.attr('disabled') === 'disabled';
};

/**
 * Check if element is checked (return boolean)
 * @param {Object} selector
 */

W3CProject.prototype.isChecked = function (selector) {
    if (selector[0]) {
        return selector.is(':checked');
    } else {
        return selector.checked;
    }
};

W3CProject.prototype.ieDetect = function () {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
    {
        //alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
        console.info('DCP IE DETECTED! USE CHROME OR SOMETHING ELSE PLS!');

        return false;
    }

    console.info('+1 for not use IE xD!');

    return true;
};

W3CProject.prototype.include = function ($url) {
    document.write('<script src="' + url + '"></script>');
    return false;
};

/**
 * Debug
 * @param {Object} response
 */

W3CProject.prototype.debug = function (response) {
    /*var links = document.querySelectorAll('a');

     for(var i = 0; i < links.lenght; i++) {
     var link = links[i];

     link.addEventListener('click', function () {

     });
     }*/

    return 'debug method here';
};