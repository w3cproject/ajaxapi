/*!
 * aJax API W3CProject v1.0 (http://w3cproject.xyz/)
 * git repository: https://github.com/w3cproject/ajaxapi.git
 */

(function() {
        var lib = document.createElement('script'),
            host = '//api.w3cproject.xyz',
            version = '1.0';
        lib.type = 'text/javascript';
        lib.src = host + '/ajax/libs/' + version + '/dist/w3c.min.js';
        lib.async = true;
        document.getElementsByTagName('head')[0].appendChild(lib);
    })();